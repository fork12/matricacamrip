package fork.matricacam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import com.jcraft.jsch.*;


public class MainActivity extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.PictureCallback {
    private int cadr=0;
    private long lasttime;
    private Timer timer;
    private int cc=0;
    private TextView timer_view;

    private static final int NO_BACK_CAMERA = -1;
    private Camera mCamera;
    private boolean mPreviewIsRunning = false;
    private boolean mIsTakingPicture = false;
    private static final String TAG = "MatricaCamrip Main";
    private SurfaceView surfaceView;
    public FrameLayout framePreview;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timer_view = (TextView) findViewById(R.id.timer);
        framePreview = (FrameLayout) findViewById(R.id.framePreview);
    }

    private void muteAudio(){
        AudioManager amanager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
        amanager.setStreamMute(AudioManager.STREAM_ALARM, true);
        amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        amanager.setStreamMute(AudioManager.STREAM_RING, true);
        amanager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
    }
    private void unMuteAudio(){
        AudioManager amanager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
        amanager.setStreamMute(AudioManager.STREAM_ALARM, true);
        amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        amanager.setStreamMute(AudioManager.STREAM_RING, true);
        amanager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
    }
    private void lockOff(){
        //отключаем лок экрана
        framePreview.setKeepScreenOn(true);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        //снижаем яркость экрана
        layoutParams.screenBrightness = 0.02f;
        getWindow().setAttributes(layoutParams);
    }

    @Override
    protected void onStart() {
        super.onStart();

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        lockOff();
        muteAudio();

        timer = new Timer("DigitalClock");
        lasttime = new Date().getTime()-280000;
        showTimer();

        final Runnable updateTask = new Runnable() {
            public void run() {
                boolean fl = false;
                long cur=new Date().getTime();
                if((cur-lasttime)>300000){
                    lasttime=cur;
                    cadr=1;
                    fl=true;
                } else if(cadr>0){
                    int diff=(int)((cur-lasttime)/14000+1);
                    if(diff>cadr) {
                        if (cadr < 5) {
                            cadr = diff;
                            fl = true;
                        } else cadr = 0;
                    }
                }
                if(fl && cc<2) {
                    //делаем фото

                    mPreviewIsRunning=false;
                    surfaceView = new CameraPreview(MainActivity.this);
                    framePreview.addView(surfaceView);
                    SurfaceHolder holder = surfaceView.getHolder();
                    if (Build.VERSION.SDK_INT < 11) // 11 = Build.VERSION_CODES.HONEYCOMB
                        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                    holder.addCallback(MainActivity.this);


//                    cc++;
                }
                showTimer();
            }
        };

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(updateTask);
            }
        }, 5000, 500);
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
        timer.purge();
        timer = null;
        unMuteAudio();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //cam shot w/o sound
        muteAudio();

    }



    @Override
    protected void onPause()
    {
        super.onPause();
        unMuteAudio();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }



    private void showTimer(){
        int ost = (int) (300000-(new Date().getTime()-lasttime));
        int min = ost/60000;
        int sec = ost%60000/1000;
        timer_view.setText(String.format("%02d : %02d",min,sec));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, Prefs.class);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        finish();
    }

    private int getFrontCameraId()
    {
        final int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++)
        {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) return i;
        }
        return NO_BACK_CAMERA;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        final int cameraId = getFrontCameraId();
        if (cameraId != NO_BACK_CAMERA)
        {
            try
            {
                mCamera = Camera.open(cameraId);

                Camera.Parameters parameters = mCamera.getParameters();
//                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
//                    parameters.setRotation(270);

                List<String> flashModes = parameters.getSupportedFlashModes();
                if (flashModes != null && flashModes.contains(Camera.Parameters.FLASH_MODE_ON))
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);

                List<String> whiteBalance = parameters.getSupportedWhiteBalance();
                if (whiteBalance != null && whiteBalance.contains(Camera.Parameters.WHITE_BALANCE_AUTO))
                    parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);

                List<String> focusModes = parameters.getSupportedFocusModes();
                if (focusModes != null)
                    if(focusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO))
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
                    else if(focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);


                List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
                if (sizes != null && sizes.size() > 0)
                {
                    int maxw=0,maxh=0;
                    for(Camera.Size c : sizes) if(c.width>maxw) {maxw=c.width; maxh=c.height;}
                    parameters.setPictureSize(maxw, maxh);
                }

                List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
                if (previewSizes != null)
                {
                    int minw=1000000,minh=0;
                    for(Camera.Size c : previewSizes) if(c.width<minw) {minw=c.width; minh=c.height;}
                    parameters.setPreviewSize(minw, minh);
                }

                mCamera.setParameters(parameters);

//                if (Build.VERSION.SDK_INT >= 17)  //17 = Build.VERSION_CODES.JELLY_BEAN_MR1
//                    mCamera.enableShutterSound(false);
            }
            catch (RuntimeException e)
            {
                Log.e(TAG, e.getMessage());
//                finish();
                return;
            }
        }
        else
        {
            Log.e(TAG, "Could not find front-facing camera");
//            finish();
            return;
        }

        try
        {
            mCamera.setPreviewDisplay(surfaceHolder);
        }
        catch (IOException ioe)
        {
            Log.e(TAG, ioe.getMessage());
//            finish();
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        releaseCamera();
    }

    private void startPreview()
    {
        if (!mPreviewIsRunning && mCamera != null)
        {
            try
            {
                mCamera.startPreview();
                mCamera.setOneShotPreviewCallback(this);
/*                mCamera.autoFocus(new Camera.AutoFocusCallback()
                {
                    @Override
                    public void onAutoFocus(boolean b, Camera camera)
                    {
                        if (!mIsTakingPicture)
                        {
                            try
                            {
                                mIsTakingPicture = true;
                                mCamera.setPreviewCallback(null);
                                mCamera.takePicture(null, null, scrActivity.this);
                            }
                            catch (RuntimeException e)
                            {
                                Log.e(TAG, e.getMessage());
                                finish();
                            }
                        }
                    }
                });*/
                mPreviewIsRunning = true;
            }
            catch (Exception e)
            {
                Log.e(TAG, e.getMessage());
//                finish();
            }
        }
    }

    private void stopPreview()
    {
        if (!mIsTakingPicture && mPreviewIsRunning && mCamera != null) {
            mCamera.stopPreview();
            mPreviewIsRunning = false;
        }
    }

    private void releaseCamera()
    {
        if (mCamera != null)
        {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        mIsTakingPicture = true;
        mCamera.takePicture(null, null, this);

    }

// jpeg
    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        mIsTakingPicture = false;
        stopPreview();

        releaseCamera();

        new SaveImageTask().execute(bytes, lasttime, cadr, pref);

        framePreview.removeView(surfaceView);
        surfaceView.getHolder().removeCallback(this);
        surfaceView=null;

    }


    public class CameraPreview extends SurfaceView
    {
        public CameraPreview(Context context)
        {
            super(context);

        }
    }

}

class SaveImageTask {
    private static final String TAG = "MatricaCamrip Save";
    private Session session;
    private Channel channel;

    private int mparse(String s, int def){
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return def;
        }

    }

    public void execute(byte[] bytes,long timestamp,int num,SharedPreferences pref){
        //crop image
        Bitmap src = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
        int x = mparse(pref.getString("pref_crop_x","0"),0);
        int y = mparse(pref.getString("pref_crop_y","0"),0);
        int w = mparse(pref.getString("pref_crop_width","0"),0);
        int h = mparse(pref.getString("pref_crop_height","0"),0);
        Bitmap dst=null;
        if(src != null && w>0 && h>0) dst = Bitmap.createBitmap(src,x,y,w,h);

        try {
            File saveDir = new File(Environment.getExternalStorageDirectory().getPath()+"/MatricaCamrip/");
            if (!saveDir.exists()) saveDir.mkdirs();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yy_KK-mm");
            String packName=sdf.format(new Date(timestamp));
            String fn = packName+"_"+num;
            String suf;
            if(dst!=null) suf=".png"; else suf=".jpg";
            FileOutputStream os = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/MatricaCamrip/"+fn+suf);
            if(dst!=null)
                dst.compress(Bitmap.CompressFormat.PNG,100,os);
            else
                os.write(bytes);
            os.close();

            if((num==5) && (!pref.getString("pref_ssh_host", "").isEmpty())){
                packPacket(packName,suf);
                sendImgPacket(packName,pref);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void packPacket(String name,String suf){
        String cmd="/system/xbin/tar -czf "+Environment.getExternalStorageDirectory().getPath()+"/MatricaCamrip/"+name+".tar.gz -C "+Environment.getExternalStorageDirectory().getPath()+"/MatricaCamrip";
        for(int i=1;i<6;i++) cmd+=" "+name+"_"+i+suf;
        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            while (in.readLine() != null) { }
//            Log.e(TAG,full);
            in.close();
            proc.destroy();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }


        cmd = Environment.getExternalStorageDirectory().getPath()+"/MatricaCamrip/"+name+"_";
        for(int i=1;i<6;i++) {
            File file = new File(cmd+i+suf); file.delete();
        }


    }

    private void sendImgPacket(String name, SharedPreferences pref) {
        String fn=Environment.getExternalStorageDirectory().getPath()+"/MatricaCamrip/"+name+".tar.gz";
        try {
            JSch jsch = new JSch();

            String host = pref.getString("pref_ssh_host", "");
            String user = pref.getString("pref_ssh_user", "");
            int port=22;
            session=jsch.getSession(user, host, port);
            session.setPassword(pref.getString("pref_ssh_pass", ""));
            Properties prop = new Properties();
            prop.put("StrictHostKeyChecking", "yes");
            session.setConfig(prop);
            jsch.setKnownHosts(Environment.getExternalStorageDirectory().getPath()+"/ssh_hosts");
            session.connect();
            channel=session.openChannel("sftp");
            channel.connect();
            ChannelSftp c=(ChannelSftp)channel;
            int mode=ChannelSftp.OVERWRITE;
            String pt = pref.getString("pref_ssh_path", ""); if(!pt.isEmpty() && !pt.endsWith("/")) pt+="/";
            c.put(fn, pt+name+".tar.gz",  mode);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (channel != null) channel.disconnect();
            if (session != null) session.disconnect();
        }
        File file = new File(fn); file.delete();
    }


}

